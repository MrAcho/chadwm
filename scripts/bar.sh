#!/bin/dash

# ^c$var^ = fg color
# ^b$var^ = bg color

interval=0

# load colors
. ~/.config/chadwm/scripts/bar_themes/catppuccin

cpu() {
	cpu_val=$(grep -o "^[^ ]*" /proc/loadavg)

	printf "^c$black^ ^b$green^ CPU"
	printf "^c$white^ ^b$grey^ $cpu_val"
}
mem(){
	printf "^c$blue^^b$black^ "
	printf "^c$blue^ $(free -h | awk '/^Mem/ { print $3 }' | sed s/i//g)"
}

clock() {
	printf "^c$black^ ^b$darkblue^  "
	printf "^c$black^^b$blue^ $(date '+%I:%M %p %A %d %b ')"
}
player() {
        if [ ! -z $(playerctl metadata xesam:title) ]; then
          printf "^c$black^ ^b$spot^ "
          printf "^c$spot^ ^b$grey^ $(playerctl metadata xesam:title)"
        fi

}
while true; do

	[ $interval = 0 ] || [ $(($interval % 3600)) = 0 ] && updates=$(pkg_updates)
	interval=$((interval + 1))

        sleep 1 && xsetroot -name "$(player) $(cpu) $(mem) $(clock)"
done
